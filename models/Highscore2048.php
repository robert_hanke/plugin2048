<?php
/**
 * Created by PhpStorm.
 * User: robert
 * Date: 18.08.15
 * Time: 14:41
 */

class Highscore2048 extends SimpleORMap {

    protected static function configure($config = array())
    {
        $config['db_table'] = 'highscore2048';

        $config['belongs_to']['user'] = array(
            'class_name' => 'User',
            'foreign_key' => 'user_id',
        );

        parent::configure($config);
    }
}