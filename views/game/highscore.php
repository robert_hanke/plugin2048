<table class="default">
    <thead>
        <tr>
            <th>Platz</th>
            <th>Name</th>
            <th>Datum</th>
            <th>Score</th>
        </tr>
    </thead>
    <tbody>
        <? for($i = 0; $i < count($scores); $i++) : ?>
            <tr>
                <td><?=$i + 1?>.</td>
                <td>
                    <a href="<?=URLHelper::getLink("dispatch.php/profile",
                        array(
                            "username"=>get_username($scores[$i]->user_id)
                        )
                    )?>">
                        <?=Avatar::getAvatar($scores[$i]->user_id)->getImageTag(Avatar::SMALL)?>
                        <?=htmlReady(get_fullname($scores[$i]->user_id))?>
                    </a>
                </td>
                <td><?=date("j.m.Y", $scores[$i]->mkdate)?></td>
                <td><?=htmlReady($scores[$i]->score)?></td>
            </tr>
        <? endfor ?>
    </tbody>
</table>