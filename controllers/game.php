<?php
/**
 * Created by PhpStorm.
 * User: robert
 * Date: 18.08.15
 * Time: 11:25
 */

require_once "app/controllers/plugin_controller.php";

class GameController extends PluginController {

    public function play_action() {
        Navigation::activateItem("/community/2048/play");
        PageLayout::addStylesheet($this->plugin->getPluginURL()."/assets/stylesheets/main.css");
        PageLayout::addScript($this->plugin->getPluginURL()."/vendor/2048/js/bind_polyfill.js");
        PageLayout::addScript($this->plugin->getPluginURL()."/vendor/2048/js/classlist_polyfill.js");
        PageLayout::addScript($this->plugin->getPluginURL()."/vendor/2048/js/animframe_polyfill.js");
        PageLayout::addScript($this->plugin->getPluginURL()."/vendor/2048/js/keyboard_input_manager.js");
        PageLayout::addScript($this->plugin->getPluginURL()."/vendor/2048/js/html_actuator.js");
        PageLayout::addScript($this->plugin->getPluginURL()."/vendor/2048/js/grid.js");
        PageLayout::addScript($this->plugin->getPluginURL()."/vendor/2048/js/tile.js");
        PageLayout::addScript($this->plugin->getPluginURL()."/vendor/2048/js/local_storage_manager.js");
        //PageLayout::addScript($this->plugin->getPluginURL()."/vendor/2048/js/game_manager.js");
        PageLayout::addScript($this->plugin->getPluginURL()."/assets/javascripts/game_manager.js");
        PageLayout::addScript($this->plugin->getPluginURL()."/vendor/2048/js/application.js");
    }

    public function save_score_action() {
        if(Request::isPost()) {
            $highscore = new Highscore2048();
            $highscore['score'] = Request::int("score");
            $highscore['user_id'] = $GLOBALS['user']->id;
            $highscore->store();
        }
        $this->render_nothing();
    }

    public function highscore_action() {
        Navigation::activateItem("/community/2048/highscore");
        $this->scores = Highscore2048::findBySQL("1=1 ORDER BY score DESC LIMIT 100");
    }
}