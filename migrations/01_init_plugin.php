<?php

class InitPlugin extends Migration {

    public function up() {
        DBManager::get()->exec("
            CREATE TABLE IF NOT EXISTS `highscore2048` (
              `highscore2048_id` VARCHAR(32) NOT NULL,
              `score` INT NOT NULL,
              `user_id` VARCHAR(32) NOT NULL,
              `mkdate` INT NOT NULL,
              PRIMARY KEY(`highscore2048_id`),
              KEY `user_id` (`user_id`)
            )
        ");
    }
}