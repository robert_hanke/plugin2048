<?php

/**
 * Created by PhpStorm.
 * User: robert
 * Date: 18.08.15
 * Time: 11:19
 */
require_once __DIR__."/models/Highscore2048.php";

class Plugin2048 extends StudIPPlugin implements SystemPlugin
{
    public function __construct(){
        parent::__construct();
        $nav = new Navigation("2048", PluginEngine::getURL($this, array(), "game/play"));
        $nav_play = new Navigation("Play", PluginEngine::getURL($this, array(), "game/play"));
        $nav_highscore = new Navigation("Highscore", PluginEngine::getURL($this, array(), "game/highscore"));
        //$nav->setImage(Assets::image_path("icons/lightblue/tan.svg"));
        //$nav->setImage($this->getPluginURL()."/assets/images/2048.svg");
        Navigation::addItem("/community/2048", $nav);
        Navigation::addItem("/community/2048/play", $nav_play);
        Navigation::addItem("/community/2048/highscore", $nav_highscore);
    }
}